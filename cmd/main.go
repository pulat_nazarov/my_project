package main

import (
	"fmt"
	"log"
)

func main() {
	fmt.Println("salom dunyo")
	fmt.Println("hello world")

	fmt.Println("second commit")
	fmt.Println("first commit")

	var a int
	fmt.Println("a is equal", a)

	log.Println("third commit")
	// some command
	a++
	// commit1
	// commit2
	// commit3
	fmt.Println()
}
